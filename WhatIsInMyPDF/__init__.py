from pathlib import Path
from WhatIsInMyPDF import Main
from flask import Flask


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_pyfile("config.py", silent=False)
        app.config.update(test_config)

    app.register_blueprint(Main.Decorateur)

    folder = Path(app.config["UPLOAD_FOLDER"])
    if False is folder.exists():
        folder.mkdir()

    return app
