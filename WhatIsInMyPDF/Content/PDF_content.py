from WhatIsInMyPDF.BD.SQLLite import session_factory, Base
from sqlalchemy import Column, Integer, String
from flask import current_app as app
from multiprocessing import Process
from PyPDF2 import PdfFileReader
from datetime import datetime
from pathlib import Path
import pdftotext
import json


#Informations sur les metadata
class AppModel(Base):

    __tablename__ = "file"
    # Metadata informations
    internal_id = None
    id = Column("id", Integer, primary_key=True)
    date = Column("date", String(255))
    raw_info = Column("raw_info", String())
    content = Column("content", String)
    subject = Column("subject", String(255))
    title = Column("title", String(255))
    number_of_pages = Column("number_of_pages", Integer)
    author = Column("author", String(255))
    status = Column("status", String(255))
    creator = Column("creator", String(255))
    producer = Column("producer", String(255))


    def __init__(
        self: object,
        status: str = None,
        date: str = None,
        author: str = None,
        creator: str = None,
        producer: str = None,
        subject: str = None,
        title: str = None,
        number_of_pages: int = None,
        raw_info: str = None,
        content: str = None,
    ):

        self.status = str(status)
        self.date = str(date)
        self.author = str(author)
        self.creator = str(creator)
        self.producer = str(producer)
        self.subject = str(subject)
        self.title = str(title)
        self.number_of_pages = number_of_pages
        self.raw_info = str(raw_info)
        self.content = str(content)

        self._output_folder = Path(app.config["DATA_FOLDER"])
        if False is self._output_folder.exists():
            self._output_folder.mkdir()

    
    def extract_and_persist(self, filename: Path):
        """Public method to extract then persist a PDF object in the database
        """
        if str(filename).rsplit(".", 1)[1].lower() == "pdf":
            object_id = self._persist()
            process = Process(target=self._async_extract_and_persist, args=(filename, object_id))
            process.start()
            return object_id

        return None

    def _persist(
        self,
        date: str = None,
        author: str = None,
        creator: str = None,
        producer: str = None,
        subject: str = None,
        title: str = None,
        number_of_pages: int = None,
        raw_info: str = None,
        content: str = None,
        object_id: int = None,
    ):
        """Private method to persist/update the object in the database"""
        session = session_factory()
        if object_id is None:
            self.status = "EN COURS"
            self.date = str(date)
            self.author = str(author)
            self.creator = str(creator)
            self.producer = str(producer)
            self.subject = str(subject)
            self.title = str(title)
            self.number_of_pages = number_of_pages
            self.raw_info = str(raw_info)
            self.content = str(content)
            session.add(self)
        else:
            article_model = session.query(AppModel).get(object_id)
            article_model.status = "REUSSI"
            article_model.date = str(date)
            article_model.author = str(author)
            article_model.creator = str(creator)
            article_model.producer = str(producer)
            article_model.subject = str(subject)
            article_model.title = str(title)
            article_model.number_of_pages = number_of_pages
            article_model.raw_info = str(raw_info)
            article_model.content = str(content)

        session.commit()
        self.internal_id = self.id
        session.close()

        return self.internal_id

    def _async_extract_and_persist(self, filename: Path, object_id: int):
        """Private method to extract then update a PDF object in the database
        """
        today = datetime.today().strftime("%Y-%m-%d-%H-%M-%S.%f")
        # Create a unique filename
        output_filepath = self._output_folder / Path("file_" + today + ".txt")

        with open(filename, "rb") as file:
            # Extraire le texte
            data = pdftotext.PDF(file)
            # Extraire la  metadata
            pdf = PdfFileReader(file)
            info = pdf.getDocumentInfo()
            number_of_pages = pdf.getNumPages()
            author = info.author
            creator = info.creator
            producer = info.producer
            subject = info.subject
            title = info.title

            with open(output_filepath, "w", encoding="utf-8") as file:
                # Saving content to a text file
                file.write("\n".join(data))
                # Saving content AND meta data to the database
                self._persist(
                    today,
                    author,
                    creator,
                    producer,
                    subject,
                    title,
                    number_of_pages,
                    info,
                    "".join(data),
                    object_id
                )
                return self.internal_id

#Convertir l'objet en JSON
class AppToJson(json.JSONEncoder):

    def default(self, a):
        if isinstance(a, AppModel):
            doc_id = a.id
            if None is doc_id:
                # Si None, l'objet a été créé après une requête INSERT alors le internal_id est l'id de la table.
                doc_id = a.internal_id
            return {
                "ID": doc_id,
                "Status": a.status,
                "Date": a.date,
                "Author": a.author,
                "Creator": a.creator,
                "Producer": a.producer,
                "Subject": a.subject,
                "Title": a.title,
                "Number_of_pages": a.number_of_pages,
                "Raw_info": a.raw_info,
                "Content": a.content,
            }
        return super().default(a)
