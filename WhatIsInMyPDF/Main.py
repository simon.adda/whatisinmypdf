from flask import  Response, render_template, Blueprint, request, Flask
from WhatIsInMyPDF.Content.PDF_content import AppModel
from WhatIsInMyPDF.BD.SQLLite import session_factory
from werkzeug.utils import secure_filename
from flask import current_app as app
from WhatIsInMyPDF import Main
from sqlalchemy import select
from pathlib import Path
import json



class Message:
    """Class for returning a generic message"""

    # Generic user message
    message = ""
    # ID of the inserted object
    object_id = None

    def __init__(self: object, message: str = None, object_id: int = None):
        self.object_id = object_id
        self.message = message

class MessageEncoder(json.JSONEncoder):
    """Class for converting full object to JSON string"""

    def default(self, o):
        if isinstance(o, Message):
            return {"id": o.object_id, "message": o.message}

        return super().default(o)

class Control:
    """Controle de l'API"""

    def __init__(self: object):
        pass

    @staticmethod
    def Flask_front(name):
        """Index de l'API.
        """
        if name.method == "POST":
            if "file" not in name.files:
                return Response(
                    json.dumps(Message("Pas de doc"), cls=MessageEncoder),
                    mimetype="application/json;charset=utf-8",
                ), 400
            file = name.files["file"]

            # If the file's type is allowed
            if file and Control._allowed_file(file.filename):
                filename = secure_filename(file.filename)
                filepath = Path().joinpath(app.config["UPLOAD_FOLDER"], filename)
                # Save the file in an upload folder
                file.save(filepath)
                # Extract and persist the file in the database
                doc_id = AppModel().extract_and_persist(filepath)
                # If failed
                if None is doc_id:
                    # Returns the appropriate error
                    return Response(
                        json.dumps(
                            Message("This file's type is not allowed!"),
                            cls=MessageEncoder,
                        ),
                        mimetype="application/json;charset=utf-8",
                    ), 400
                return Response(
                    json.dumps(
                        Message(
                            "The file '" + filename + "' has been sent successfully!",
                            doc_id,
                        ),
                        cls=MessageEncoder,
                    ),
                    mimetype="application/json;charset=utf-8",
                ), 201

        message = name.args.get("message")
        if None is message:
            message = ""

        # Generate an front HTML
        return render_template("index.html", title="page", message=message)

    @staticmethod
    def doc_content(request, doc_id: int):
        """Information sur le document
        """
        if request.method == "GET":
            # Preparing the query for the ID
            stmt = select(AppModel).where(AppModel.id == doc_id)
            # Retreive the session
            session = session_factory()
            # Executing the query
            result = session.execute(stmt)
            # Parsing the result
            for user_obj in result.scalars():
                # We build the JSON response
                data = {}
                data["author"] = user_obj.author
                data["creator"] = user_obj.creator
                data["subject"] = user_obj.subject
                data["title"] = user_obj.title
                data["number_of_pages"] = user_obj.number_of_pages
                data["id"] = user_obj.id
                data["status"] = user_obj.status
                # Converting to JSON string
                data_js=json.dumps(data)
                return Response(data_js, mimetype="application/json;charset=utf-8")
            # Else, no document found
            return Response(
                json.dumps(Message("Pas de document"), cls=MessageEncoder),
                mimetype="application/json;charset=utf-8",
            ), 404
        return Response(
            json.dumps(Message("Mauvais HTTP"), cls=MessageEncoder),
            mimetype="application/json;charset=utf-8",
        ), 405

    @staticmethod
    def pdf_content(request, doc_id: int):
        """Content of a document.
        """
        if request.method == "GET":
            # Preparing the query for the ID
            stmt = select(AppModel).where(AppModel.id == doc_id)
            # Retreive the session
            session = session_factory()
            # Executing the query
            result = session.execute(stmt)
            # Parsing the result
            for user_obj in result.scalars():
                data = {}
                data["content"] = user_obj.content
                # Convert en JSON string
                data_js = json.dumps(data)
                return Response(data_js, mimetype="application/json;charset=utf-8")
            # Else, no document found
            return Response(
                json.dumps(Message("Pas de document"), cls=MessageEncoder),
                mimetype="application/json;charset=utf-8",
            ), 404
        return Response(
            json.dumps(Message("Mauvais HTTP"), cls=MessageEncoder),
            mimetype="application/json;charset=utf-8",
        ), 405


#Les routes d'acces au informations du pdf
Decorateur = Blueprint("router", __name__, template_folder="templates")

@Decorateur.route("/", methods=["GET", "POST"])
@Decorateur.route("/documents", methods=["GET", "POST"])
def Flask_front():
    return Control.Flask_front(request)
@Decorateur.route("/documents/<int:doc_id>", methods=["GET"])
def doc_content(doc_id):
    return Control.doc_content(request, doc_id)
@Decorateur.route("/text/<int:doc_id>", methods=["GET"])
def pdf_content(doc_id):
    return Control.pdf_content(request, doc_id)