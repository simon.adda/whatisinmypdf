from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

name = create_engine("sqlite:///instance/pdfextractor.db")
_SessionFactory = sessionmaker(bind=name)

Base = declarative_base()

def session_factory():
    Base.metadata.create_all(name)
    return _SessionFactory()
