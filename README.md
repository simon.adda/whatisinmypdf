# WhatIsInMyPDF

Ici, vous trouverez une application Flask qui vous permettra d'extraire le texte et les metadonées d'un pdf.

# Installation

We recommend using the latest version of Python. Flask supports Python 3.6 and newer.

## Avec Linux

### Dependencies: pdftotext

#### Debian, Ubuntu

    $ sudo apt install build-essential libpoppler-cpp-dev pkg-config python3-dev

#### Fedora, Red Hat

    $ sudo yum install gcc-c++ pkgconfig poppler-cpp-devel python3-devel


#### macOS

    brew install pkg-config poppler python

### WhatIsInMyPDF. Comment ca marche

Créer un virtualenv et l'activer :

    $ python -m venv venv
    $ . venv/bin/activate

Installer PdfExtractor:

    $ pip install -r requirements.txt

# Run

## Avec Linux or Mac OS

    $ export FLASK_APP=WhatIsInMyPDF
    $ flask run

# Usage

Ouvrez http://localhost:5000 dans un navigateur pour essayer le logiciel ou utilisez l'API suivante.


### Utilisation de SQlAlchemy pour stocker les id et infos des pdf

SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.

It provides a full suite of well known enterprise-level persistence patterns, designed for efficient and high-performing database access, adapted into a simple and Pythonic domain language.


## Specification API 

### Charger un fichier

Poster un fichier sur le serveur: 

**Requete**

    HTTP Methode: POST
    Route: /
    Alternate: /documents
    # La méthode GET renvoie un formulaire de téléchargement HTML.
    # La méthode POST peut être utilisée pour télécharger un fichier PDF.

**Reponse**

    {
        # ID of the uploaded PDF file, otherwise 'null' if error (int)
        "id": [id],
        # Explicit human readable status message (str)
        "message": "[message]"}"
    }


#### Example:
     
     curl -F 'file=@monpdf.pdf' localhost:5000

### Verifier l'importation du fichier

Obtenez le statut d'un fichier téléchargé et afficher ses méta-données :

**Requete**

    HTTP Methode: GET
    Route: /documents/{id}
    # Informations sur un document.
    # La méthode GET renvoie des métadonnées sur le document, spécifié par le paramètre ID.

**Reponse**

    {
        # ID of the uploaded PDF file, otherwise 'null' if not found (int)
        "author": "[author]",
        "creator": "[creator]",
        "subject": "[subject]", 
        "title": "[title]", 
        "number_of_pages": [number_of_pages], 
        "id": [id], 
        "status": "[status]"
    }

Exemple:
    {"author": "", "creator": "LaTeX with hyperref", "subject": "", "title": "", "number_of_pages": 18, "id": 21, "status": "REUSSI"}


### Texte du fichier

Affichier dans la page web le text du PDF:
**Requete**

    HTTP Methode: GET
    Route: /text/{id}
    # Contenu d'un document en entrant l'id.
    # La méthode GET retourne le contenu d'un document.

**Reponse**

    {
        # Content of the specified PDF document (str)
        "content": [content]
    }

Exemple: 
    {"content": "                                             A Methodology for Developing a Verifiable Aircraft Engine\n                                                     Controller from Formal Requirements }


## Les diffenrents packages installés (requirements.txt)
    py==1.10.0
    pyparsing==3.0.1
    PyPDF2==1.26.0
    pytest==6.2.5
    SQLAlchemy==1.4.26
    toml==0.10.2
    Werkzeug==2.0.2
    attrs==21.2.0
    click==8.0.3
    Flask==2.0.2
    greenlet==1.1.2
    iniconfig==1.1.1
    itsdangerous==2.0.1
    Jinja2==3.0.2
    MarkupSafe==2.0.1
    packaging==21.0
    pluggy==1.0.0
    pdftotext==2.2.1