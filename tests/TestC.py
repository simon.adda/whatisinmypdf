import pytest
from WhatIsInMyPDF import create_app


@pytest.fixture
def client():
    app = create_app({"TESTING": True})

    with app.test_client() as customer:
        yield customer
