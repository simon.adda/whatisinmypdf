# Dossier ou les infos sont stockés
UPLOAD_FOLDER = 'uploads'
# Que des pdf
ALLOWED_EXTENSIONS = {'pdf'}
# Copy de la données en local dans le dossier data
DATA_FOLDER = 'data'
